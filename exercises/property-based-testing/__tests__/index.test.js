const fc = require('fast-check');

const asc = (a, b) => a - b;
const desc = (a, b) => b - a;

// BEGIN
describe('Array.sort', () => {
  it('sort number', () => {
    fc.assert(fc.property(fc.int32Array(), (array) => {

      expect(array.sort(asc)).toBeSorted();
      expect(array.sort(desc)).toBeSorted({ descending: true });
    }));
  });

  it('should idempotent', () => {
    fc.assert(fc.property(fc.int16Array(), (array) => {
      expect(array.sort().sort()).toEqual(array.sort());
    }));
  });

  it('should sort date', () => {
    fc.assert(fc.property(fc.array(fc.date()), (array) => {
      expect(array.sort(asc)).toBeSorted();
    }));
  });

  it('should true', () => {
    expect([].sort()).toEqual([]);
  });

});
// END
