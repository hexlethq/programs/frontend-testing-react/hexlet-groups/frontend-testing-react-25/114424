const fs = require('fs');

// BEGIN
const updateType = {
  MAJOR: 'major',
  MINOR: 'minor',
  PATCH: 'patch',
}
const UNICODE = 'utf-8';

const increase = (data) => {
  const number = Number.isNaN(data) ? 0 : data;
  return number + 1;
};

const upVersion = (path, type = updateType.PATCH) => {
  let data;
  try {
    const content = fs.readFileSync(path, UNICODE);
    data = JSON.parse(content);
  } catch (error) {
    throw new Error(`Error: ${error}`)
  }
  if (typeof data.version !== 'string') {
    throw new Error(`invalid version`);
  }
  let [major, minor, patch] = data.version.split('.').map(Number);
  switch (type) {
    case updateType.PATCH: {
      patch = increase(patch);
      break;
    }
    case updateType.MINOR: {
      minor = increase(minor);
      patch = 0;
      break;
    }
    case updateType.MAJOR: {
      major = increase(major);
      minor = 0;
      patch = 0;
      break;
    }
  }
  data.version = `${major}.${minor}.${patch}`;
  fs.writeFileSync(path, JSON.stringify(json, null, 2), UNICODE);
  return json.version;
};
// END

module.exports = { upVersion };
