describe('Object.assign', () => {
  test('checking the merge', () => {
    const first = { k: 'v', b: 'b', c: 777 };
    const second = { k: 'v2', a: 'a' };
    const third = { k: 'qwer', c: 123 };

    const resultFirst = Object.assign(first, second);
    const resultSecond = Object.assign({}, first, second, third);

    expect(resultFirst).toEqual({ k: 'v2', a: 'a', b: 'b', c: 777 });
    expect(resultSecond).toEqual({ k: 'qwer', a: 'a', b: 'b', c: 123 });
  });

  test('only the first object changes', () => {
    const first = { k: 'v', b: 'b', c: 777 };
    const second = { k: 'v2', a: 'a' };
    const third = { k: 'qwer', c: 123 };

    const result = Object.assign(first, second, third);

    expect(first).toEqual(result);
    expect(first).toBe(first);
    expect(second).toEqual({ k: 'v2', a: 'a' });
    expect(third).toEqual({ k: 'qwer', c: 123 });
  });

  test('inherited and non-enumerable properties are not copied', () => {
    const obj = Object.create({ foo: 1 }, { // foo является унаследованным свойством.
      bar: {
        value: 2  // bar является неперечисляемым свойством.
      },
      baz: {
        value: 3,
        enumerable: true  // baz является собственным перечисляемым свойством.
      }
    });

    const result = Object.assign({}, obj);

    expect(result).toEqual({ baz: 3 });
  });

  test('copying access methods', () => {
    const obj = {
      foo: 1,
      get bar() {
        return 2;
      }
    };

    const result = Object.assign({}, obj);

    expect(result).toEqual({ foo: 1, bar: 2 });
  });

  test('change writable: false -> throw', () => {
    const target = Object.defineProperty({}, 'foo', {
      value: 1,
      writable: false
    });

    expect(() => Object.assign(target, { foo: 2 })).toThrow();
  });

  test('first parametr null -> throw', () => {
    expect(() => Object.assign(null, { foo: 2 })).toThrow();
  });

});

